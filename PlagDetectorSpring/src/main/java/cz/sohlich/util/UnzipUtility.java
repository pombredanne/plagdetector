package cz.sohlich.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Frantisek Spacek
 */
public abstract class UnzipUtility {

    /**
     * test if file is zip
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static boolean isZip(File file) throws IOException {

        byte[] bytes = IOUtils.toByteArray(new FileInputStream(file), 4);
        return Byte.compare(bytes[0], (byte) 0x50) == 0 && Byte.compare(bytes[1], (byte) 0x4B) == 0;
    }

    /**
     * unzip file to destination
     *
     * @param zip - zip file
     * @param extractTo - destination file
     * @throws IOException
     * @throws java.util.zip.ZipException
     */
    public static final void unzip(File zip, File extractTo) throws IOException,ZipException {
        ZipFile archive = new ZipFile(zip);
        Enumeration e = archive.entries();
        while (e.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            File file = new File(extractTo, entry.getName());
            if (entry.isDirectory() && !file.exists()) {
                file.mkdirs();
            } else {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                BufferedOutputStream out;
                try (InputStream in = archive.getInputStream(entry)) {
                    out = new BufferedOutputStream(
                            new FileOutputStream(file));
                    byte[] buffer = new byte[8192];
                    int read;
                    while (-1 != (read = in.read(buffer))) {
                        out.write(buffer, 0, read);
                    }
                }
                out.close();
            }
        }
    }
}
