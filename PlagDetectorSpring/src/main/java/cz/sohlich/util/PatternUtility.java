/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.util;

import cz.sohlich.util.constant.LangFileExtension;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Radek
 */
public abstract class PatternUtility {
    
    public static String relativeFilePathInDirectory(File file,File folder) throws IOException{
        
    if(!folder.isDirectory()) throw new IOException("Folder argument is not a folder");
    
    //OK compare paths
    String filePath = file.getAbsolutePath();
    String folderPath = folder.getAbsolutePath();
    if(!file.getAbsolutePath().contains(folder.getAbsolutePath()))throw new IOException("File not in folder");
    return filePath.replace(folderPath, "");
    }
    
    public static String[] extensionByLanguage(String lang){
    
        switch(lang.toUpperCase()){
            case "JAVA":
                return LangFileExtension.JAVA;
            case "C":
                return LangFileExtension.C;
            default:
                return LangFileExtension.NONE;
        }
        
    
    }
    
    
    
}
