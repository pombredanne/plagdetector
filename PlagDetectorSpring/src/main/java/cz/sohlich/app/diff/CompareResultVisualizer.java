/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.diff;

import cz.fai.utb.algorithms.gst.Match;
import cz.sohlich.app.entity.dao.CompareResult;
import cz.sohlich.app.entity.dao.File;
import cz.sohlich.app.repository.ComparisonRepository;
import cz.sohlich.app.repository.FileRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author radek
 */
@Component
public class CompareResultVisualizer {

    @Autowired FileRepository fileRepo;
    @Autowired ComparisonRepository compareRepository;

    public List<String> retrieveCommonSections(String original, String copy) throws IOException {

        //Obtain common part
        CompareResult comparison = compareRepository.findByFile(new String[]{original, copy});

        File originalFile = fileRepo.findOne(original);
        
        String content =IOUtils.toString(originalFile.getFile(), "UTF-8");
        
        List<String> similarityContent = new ArrayList<>();
        
        for (Match col : comparison.getMatches()) {
            int start = col.getPatternStart();
            int tokenIndexStart = originalFile.getIndexSequence()[start];
            int tokenIndexEnd = originalFile.getIndexSequence()[start+col.getLength()-1];
            

            //similarityContent.add(content.substring(tokenIndexStart, tokenIndexEnd));
        }
        
        
        

        return similarityContent;
    }

}
