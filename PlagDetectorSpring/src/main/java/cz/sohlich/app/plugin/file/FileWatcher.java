/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.plugin.file;

import cz.sohlich.app.plugin.event.PluginLoadedEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import static java.util.concurrent.TimeUnit.SECONDS;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 *
 * @author radek
 */
@Component
public class FileWatcher implements ApplicationEventPublisherAware {

    Logger log = LoggerFactory.getLogger(FileWatcher.class);
    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> watcherHandler;

    private ApplicationEventPublisher eventPublisher;
   

    /**
     * Creates a WatchService and registers the given directory
     */
    @Autowired
    public FileWatcher(@Value("${pg.plugin.path}") String path) throws IOException {
        Path dir = new File(path).toPath();
        this.watcher = FileSystems.getDefault().newWatchService();
        WatchKey key = dir.register(watcher, ENTRY_CREATE);
        this.keys = new HashMap<>();
        keys.put(key, dir);
    }

    @PostConstruct
    public void executeScheduledThread() {
        watcherHandler = scheduler.scheduleAtFixedRate(new FileWatcherThread(watcher, keys, eventPublisher), 1, 1, SECONDS);
    }

 
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher aep) {
        this.eventPublisher = aep;
    }

    @PreDestroy
    public void preDestroy() {
        if (watcherHandler != null) {
            watcherHandler.cancel(true);
        }

        if (watcher != null) {
            try {
                watcher.close();
            } catch (IOException ex) {
                log.error("Error while closing FileWatcher", ex);
            }
        }

    }

}
