/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.mongo;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Radek
 */
@Component
public class MongoConnectionFactory {

    Mongo mongoConnection;
    @Value("${pg.mongo.host}") String mongoHost;
    @Value("${pg.mongo.db}") String mongoDB;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    private void init() {

        try {
            mongoConnection = new MongoClient(mongoHost);
        } catch (UnknownHostException ex) {
            logger.error("Mongo db init connection failed", ex);
        }
    }

    @PreDestroy
    public void destroy() {
        mongoConnection.close();
    }

    public Mongo createConnection() {
        return mongoConnection;
    }

    public String getDB() {
        return mongoDB;
    };
    

}
