/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository.impl;

import cz.sohlich.app.entity.dao.File;
import cz.sohlich.app.repository.ClusterRepositoryCustom;
import cz.sohlich.app.repository.FileRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 *
 * @author Radek
 */
public class ClusterRepositoryImpl implements ClusterRepositoryCustom {

    @Autowired MongoTemplate operations;
    @Autowired FileRepository fileRepo;

    
    @Override
    public Double findMaxSimilarityBySubmission(String submissionId) {
        List<File> fileList = fileRepo.findBySubmissionId(submissionId);
        List<String> idList = fileList.stream().map(m -> m.getFileId()).collect(Collectors.toCollection(ArrayList::new));

        Aggregation agg = newAggregation(
                project("assignment", "file", "similarity"),
                unwind("file"),
                project("file", "similarity"),
                match(where("file").in(idList)),
                group("file").max("similarity").as("similarity")
        );
        AggregationResults<Map> results = operations.aggregate(agg, "compareResult", Map.class);

        Double max = Double.NEGATIVE_INFINITY;
        for (Map result : results) {
            Double val = (result.get("similarity") instanceof Double ? (Double)result.get("similarity") : Double.NaN);
            if (max.compareTo(val) < 0) {
                max = val;
            }
        }

        return max;
    }

//        db.cluster.aggregate( [ 
//    { "$unwind" : "$results"} , 
//    { "$project" : { "assignment" : 1 , "submission" : "$results.submission" , "similarity" : "$results.similarity"}} , 
//    { "$match" : { "assignment" : "54f205ff3ef60a124d826699"}},
//    { "$unwind" : "$submission"} ,
//    { "$group" : { "_id" : "$submission" , "similarity" : { "$max" : "$similarity"}}}
//    ])
    
    
    @Override
    public List findMaxSimilarityForSubmissions(String assignmentId) {
        Aggregation agg = newAggregation(
                project("assignment", "submission", "similarity"),
                match(where("assignment").is(assignmentId)),
                unwind("submission"),
                project("submission", "similarity"),
                group("submission").max("similarity").as("similarity")
        );
        
        AggregationResults<Map> results = operations.aggregate(agg, "compareResult", Map.class);
        return results.getMappedResults();
    }

    @Override
    public List findAllSimilarityForSubmission(String submissionId) {
        Aggregation agg = newAggregation(
                match(where("submission").in(submissionId)),
                unwind("submission"),
                match(where("submission").not().in(submissionId)),
                project("submission", "similarity"),
                group("submission").max("similarity").as("similarity")
        );
        AggregationResults<Map> results = operations.aggregate(agg, "compareResult", Map.class);
        return results.getMappedResults();
    }

}
