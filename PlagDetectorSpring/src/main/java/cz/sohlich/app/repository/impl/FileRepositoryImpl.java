/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository.impl;

import com.mongodb.BasicDBObject;
import cz.fai.utb.data.dto.SimplifiedFileDTO;
import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.CompareResult;
import cz.sohlich.app.entity.dao.File;
import cz.sohlich.app.repository.FileRepositoryCustom;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.BasicQuery;
import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 *
 * @author Radek
 */
public class FileRepositoryImpl implements FileRepositoryCustom {

    @Autowired
    MongoTemplate operations;

    /**
     * Get submission count by assignment.
     *
     * @param assignments
     * @return
     */
    @Override
    public List getSubmissionStatisticsByAssignments(List<Assignment> assignments) {
        //Extract assignmentIds
        List<String> idList = assignments.stream().map(m -> m.getAssignmentId()).collect(Collectors.toCollection(ArrayList::new));;
        Aggregation agg = newAggregation(
                project("assignmentId", "submissionId", "createdOn").and("createdOn").extractDayOfYear().as("dayOfYear").and("createdOn").extractYear().as("year"),
                match(where("assignmentId").in(idList)),
                group("assignmentId", "submissionId", "dayOfYear", "year").count().as("count"),
                project("assignmentId", "submissionId", "dayOfYear", "year"),
                group("assignmentId", "dayOfYear", "year").count().as("count")
        );
        AggregationResults<Map> results = operations.aggregate(agg, "file", Map.class);
        return results.getMappedResults();
    }

    /**
     * Get suspected count by assignment.
     *
     * @param assignments
     * @return
     */
    @Override
    public List getSuspectedCountByAssignments(List<Assignment> assignments) {
        //Extract assignmentIds
        List<String> idList = assignments.stream().map(m -> m.getAssignmentId()).collect(Collectors.toCollection(ArrayList::new));;
        Aggregation agg = newAggregation(
                project("assignmentId", "suspected"),
                match(where("assignmentId").in(idList)),
                match(where("suspected").is(true)),
                group("assignmentId", "suspected").count().as("count")
        );
        AggregationResults<Map> results = operations.aggregate(agg, "file", Map.class);
        return results.getMappedResults();
    }

    /**
     * Get suspected count by assignment.
     *
     * @param assignments
     * @return
     */
    @Override
    public List getSubmissionsByAssignment(String assignment) {
        //Extract assignmentIds
        Aggregation agg = newAggregation(
                match(where("assignmentId").in(assignment)),
                group("submissionId")
        );
        AggregationResults<Map> results = operations.aggregate(agg, "file", Map.class);
        return results.getMappedResults();
    }

    /**
     * Get suspected count by assignment.
     *
     * @param assignments
     * @return
     */
    @Override
    public List<SimplifiedFileDTO> getClusterableFileByAssignment(String assignment) {
        //Extract assignmentIds
        Aggregation agg = newAggregation(
                match(where("assignmentId").in(assignment)),
                project("$id", "clusterAttributes")
        );
        AggregationResults<SimplifiedFileDTO> results = operations.aggregate(agg, "file", SimplifiedFileDTO.class);
        return results.getMappedResults();
    }

    @Override
    public List getSimilarFiles(String assignment) {

        Aggregation agg = newAggregation(
                match(where("assignmentId").in(assignment)),
                group("sequence").addToSet("_id").as("file")
                .count().as("arrSize")
                .addToSet("submissionId").as("submission")
                .first("assignmentId").as("assignment"),
                project("file", "arrSize", "submission", "assignment").andExclude("_id").and("arrSize").divide("arrSize").as("similarity"),
                match(where("arrSize").gte(2))
        //                project("ids","submissions")
        );

        AggregationResults<CompareResult> results = operations.aggregate(agg, "file", CompareResult.class);
        return results.getMappedResults();
    }

    @Override
    public List getAllSimilarities(String fileId) {

        File dbFile = operations.findOne(new BasicQuery(new BasicDBObject("_id", new ObjectId(fileId))), File.class);

        Aggregation agg = newAggregation(
                project("sequence", "_id").and("sequence").project("size").as("sequenceSize"),
                unwind("sequence"),
                group("_id", "sequence", "sequenceSize").count().as("count"),
                group("sequence").push(new BasicDBObject("id", "$_id._id").append("count", "$count").append("sequenceSize", "$_id.sequenceSize")).as("records"),
                match(where("_id").in((Object[]) dbFile.getSequence())),
                unwind("records"),
                group("records.id", "records.sequenceSize").sum("records.count").as("total"),
                project("_id", "total", "sequenceSize"),
                project("_id", "total").and("sequenceSize")
                .plus(dbFile.getSequence().length).as("divider").and("total").divide(dbFile.getSequence().length).as("similarcontent"),
                project("_id", "total", "divider", "similarcontent"),
                project("_id", "total", "similarcontent").and("divider").minus("total").as("divider2"),
                project("_id", "total", "divider2", "similarcontent"),
                project("_id", "similarcontent").and("total").divide("divider2").as("jaccardcoeficient"),
                project("_id", "jaccardcoeficient", "similarcontent"),
                sort(Sort.Direction.DESC, "jaccardcoeficient"),
                match(where("similarcontent").gte(1))
        //                ,
        //                project("_id").and("jaccarddistance").multiply(-1).as("minusjaccard"),
        //                project("_id","minusjaccard"),
        //                project("_id").and("minusjaccard").plus(1).as("similarity")
        );

        AggregationResults<Map> results = operations.aggregate(agg, "file", Map.class);
        return results.getMappedResults();
    }

    @Override
    public List findByAssignmentId(String assignmentId) {

        Aggregation agg = newAggregation(
                match(where("assignmentId").in(assignmentId)),
                group("sequence").addToSet("_id").as("file")
                .count().as("arrSize")
                .addToSet("submissionId").as("submission")
                .first("assignmentId").as("assignment"),
                project("file", "arrSize", "submission", "assignment").andExclude("_id").and("arrSize").divide("arrSize").as("similarity"),
                match(where("arrSize").gte(2)),
                unwind("file"),
                group("file")
        );

        AggregationResults<Map> results = operations.aggregate(agg, "file", Map.class);

        
        List<ObjectId> similars = new ArrayList<>();
        for (Map result : results) {
            similars.add(new ObjectId(String.valueOf(result.get("_id"))));
        }
        
        
        agg = newAggregation(
                project("_id", "submissionId", "owner", "footprint", "sequence"),
                match(where("_id").not().in(similars))
                
        );
        AggregationResults<File> finalResults = operations.aggregate(agg, "file", File.class);
        return finalResults.getMappedResults();
    }
}
