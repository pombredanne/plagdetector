/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository;

import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dao.File;
import java.util.List;

/**
 *
 * @author Radek
 */
public interface FileRepositoryCustom {
    
    public List getSubmissionStatisticsByAssignments(List<Assignment> assignments);
    public List getSuspectedCountByAssignments(List<Assignment> assignments);
    public List getSubmissionsByAssignment(String assignment);
    public List getClusterableFileByAssignment(String assignment);
    public List getSimilarFiles(String assignment);
    public List getAllSimilarities(String assignment);
    public List findByAssignmentId(String assignmentId);
}
