/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository;

import cz.sohlich.app.entity.dao.CompareResult;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 *
 * @author radek
 */
public interface ComparisonRepository extends MongoRepository<CompareResult, String>{
    public List<CompareResult> deleteByAssignment(String assignment);
    public CompareResult findByFile(String[] files);
}
