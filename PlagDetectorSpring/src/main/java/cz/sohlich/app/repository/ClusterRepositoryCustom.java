/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository;

import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Radek
 */
@Repository
public interface ClusterRepositoryCustom{

    public Double findMaxSimilarityBySubmission(String submissionId);
    public List findMaxSimilarityForSubmissions(String assignmentId);
    public List findAllSimilarityForSubmission(String assignmentId);
}
