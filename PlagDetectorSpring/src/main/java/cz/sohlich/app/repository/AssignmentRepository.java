/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.repository;

import cz.sohlich.app.entity.dao.Assignment;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Radek
 */
@Repository
public interface AssignmentRepository extends MongoRepository<Assignment, String>,AssignmentRepositoryCustom {
        
    public List<Assignment> findByOwner(String username);

    public List<Assignment> findByStatus(String status);

    public Optional<Assignment> findOneByAssignmentId(String id);

    @Query(value = "{'owner': ?0}", count = true)
    public Long countByOwner(String owner);
    
}
