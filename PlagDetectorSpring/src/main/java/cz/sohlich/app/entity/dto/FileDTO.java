/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.entity.dto;

import cz.sohlich.app.entity.dao.File;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author Radek
 */
public class FileDTO {

    private String fileId;

    String filename;
    String assignmentId;
    String submissionId;
    String owner;
    byte[] file;
    Date createdOn;
    Map footprint;
    Integer[] sequence;
    Boolean template;

    public FileDTO(File entity) {
        this.fileId = entity.getFileId();
        this.assignmentId = entity.getAssignment();
        this.submissionId = entity.getSubmissionId();
        this.filename = entity.getFilename();
        this.owner = entity.getOwner();
        this.file = entity.getFile();
        this.createdOn = entity.getCreatedOn();
        this.sequence = entity.getSequence();
        this.footprint = entity.getFootprint();
        this.template = entity.isTemplate();
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getAssignment() {
        return assignmentId;
    }

    public void setAssignment(String assignment) {
        this.assignmentId = assignment;
    }

    public String getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(String submissionId) {
        this.submissionId = submissionId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Map getFootprint() {
        return footprint;
    }

    public void setFootprint(Map footprint) {
        this.footprint = footprint;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public Integer[] getSequence() {
        return sequence;
    }

    public void setSequence(Integer[] sequence) {
        this.sequence = sequence;
    }

    public Boolean isTemplate() {
        return template;
    }

    public void setTemplate(Boolean template) {
        this.template = template;
    }

}
