/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.entity.dao;

import cz.sohlich.util.constant.AssignmentStatus;
import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
/**
 *
 * @author Radek
 */
public class Assignment {

    @Id
    private String assignmentId;

    String owner;
    String lang;
    String name;
    String status;
    
    @DateTimeFormat(iso = ISO.DATE_TIME)
    Date createdOn;
    
    

    public Assignment() {
        status = AssignmentStatus.NEW;
    }

    public Assignment(String owner, String lang, String name) {
        this.owner = owner;
        this.lang = lang;
        this.name = name;
        status = AssignmentStatus.NEW;
    }

    public Assignment(String assignmentId, String owner, String lang, String name, Date createdOn) {
        this.assignmentId = assignmentId;
        this.owner = owner;
        this.lang = lang;
        this.name = name;
        this.createdOn = createdOn;
    }

    public Assignment(String assignmentId, String owner, String lang, String name, String status, Date createdOn) {
        this.assignmentId = assignmentId;
        this.owner = owner;
        this.lang = lang;
        this.name = name;
        this.status = status;
        this.createdOn = createdOn;
    }
    

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
    
    public void setCreatedNow() {
        this.createdOn = new Date();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    
}
