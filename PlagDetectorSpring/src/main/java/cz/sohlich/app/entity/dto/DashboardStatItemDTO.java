/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.entity.dto;

import java.util.List;

/**
 *
 * @author Radek
 */
public class DashboardStatItemDTO {
    
    List assignments;
    List submissions;
    List suspecteds;

    public List getAssignments() {
        return assignments;
    }

    public void setAssignments(List assignments) {
        this.assignments = assignments;
    }

    public List getSubmissions() {
        return submissions;
    }

    public void setSubmissions(List submissions) {
        this.submissions = submissions;
    }

    public List getSuspecteds() {
        return suspecteds;
    }

    public void setSuspecteds(List suspecteds) {
        this.suspecteds = suspecteds;
    }
    
    
    
}
