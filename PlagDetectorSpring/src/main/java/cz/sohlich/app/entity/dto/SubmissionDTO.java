/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.entity.dto;

/**
 *
 * @author radek
 */
public class SubmissionDTO {

    String owner;
    String assignmentId;
    String identificator;

    public SubmissionDTO() {
    }

    public SubmissionDTO(String owner, String assignmentId, String identificator) {
        this.owner = owner;
        this.assignmentId = assignmentId;
        this.identificator = identificator;
    }


    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getIdentificator() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }

    @Override
    public String toString() {
        return "SubmissionDTO{" + "owner=" + owner + ", assignmentId=" + assignmentId + ", identificator=" + identificator + '}';
    }
    
    
    
}
