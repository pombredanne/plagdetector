/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.entity.dao;

import cz.fai.utb.algorithms.gst.Match;
import org.springframework.data.annotation.Id;

import java.util.List;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * @author Radek
 */
public class CompareResult {

    @Id
    String id;
    
    @Indexed
    String assignment;
    
    String[] file = new String[2];
    String[] submission = new String[2];
    List<Match> matches;

    double similarity;

    public CompareResult() {
    }

    public CompareResult(File firstFile, File secondFile, double similarity) {
        this.file[0] = firstFile.getFileId();
        this.file[1] = secondFile.getFileId();
        this.submission[0] = firstFile.getSubmissionId();
        this.submission[1] = secondFile.getSubmissionId();
        this.similarity = similarity;
    }

    public CompareResult(File firstFile, File secondFile, double similarity, List<Match> matches, String assignmentId) {
        this.file[0] = firstFile.getFileId();
        this.file[1] = secondFile.getFileId();
        this.submission[0] = firstFile.getSubmissionId();
        this.submission[1] = secondFile.getSubmissionId();
        this.similarity = similarity;
        this.matches = matches;
        this.assignment = assignmentId;
    }

    public String[] getFile() {
        return file;
    }

    public void setFile(String[] file) {
        this.file = file;
    }

    public String[] getSubmission() {
        return submission;
    }

    public void setSubmission(String[] submission) {
        this.submission = submission;
    }

    public double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(double similarity) {
        this.similarity = similarity;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public String getAssignmentId() {
        return assignment;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignment = assignmentId;
    }

    @Override
    public String toString() {
        return "CompareResult{" + "id=" + id + ", assignment=" + assignment  + ", matches=" + matches + ", similarity=" + similarity + '}';
    }


    
    
}
