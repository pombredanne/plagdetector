/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.health;

import com.codahale.metrics.health.HealthCheck;
import com.mongodb.CommandResult;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author radek
 */
public class MongoHealthCheck extends HealthCheck {

    MongoTemplate template;

    public MongoHealthCheck(MongoTemplate template) {
        this.template = template;
    }

    @Override
    protected Result check() throws Exception {

        Result result;
        try {
            CommandResult mongoResult = template.executeCommand("{ buildInfo: 1 }");
            result = Result.healthy("{'version':'%s'}",mongoResult.getString("version"));
        } catch (Exception e) {
            result = Result.unhealthy(e);
        }
        return result;
    }

}
