/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.integration.apac;

import cz.sohlich.app.repository.ClusterRepository;
import cz.sohlich.app.repository.FileRepository;
import java.net.ConnectException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author radek
 */
@Component
public class ApacClient {

    @Autowired FileRepository fileRepo;
    @Autowired ClusterRepository clusterRepository;

    Logger logger = LoggerFactory.getLogger(ApacClient.class);

    @Value("${apac.sync.url:http://195.178.90.55:8080/sync/plagiarism}")
    private String baseUrl;

    RestTemplate restTemplate;

    public ApacClient() {
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
    }

    public boolean synchronize(String assignmentId) {

        boolean returnValue = false;
        
        List entityWrapper = new ArrayList();

        //Find similarities by assignment
        List submissions = fileRepo.getSubmissionsByAssignment(assignmentId);

        for (Object submission : submissions) {
            Map simMap = (Map) submission;
            List similarities = clusterRepository.findAllSimilarityForSubmission((String) simMap.get("_id"));
            RequestEntity submissionSimilarity = buildRequestEntity((String) simMap.get("_id"), similarities);
            entityWrapper.add(submissionSimilarity);
        }

        try {
            logger.info("Posting synchronization to APAC entity {}", Arrays.toString(entityWrapper.toArray()));
            postSimilarity(entityWrapper);
            returnValue = true;
        } catch (URISyntaxException | ConnectException ex) {
            logger.error("APAC synchronization error", ex);
        }
        
        return returnValue;
    }

    private RequestEntity buildRequestEntity(String submissionId, List similarities) {
        List simRequestList = new ArrayList();
        RequestEntity requestEntity = new RequestEntity(submissionId, simRequestList);

        for (Object similarity : similarities) {
            Map simMap = (Map) similarity;
            Double entitySimilarity = (Double) simMap.get("similarity");
            SimilarityEntity entity = new SimilarityEntity(String.valueOf(simMap.get("_id")), entitySimilarity);
            simRequestList.add(entity);
            if (entitySimilarity > requestEntity.similarity) {
                requestEntity.similarity = entitySimilarity;
            }
        }
        return requestEntity;
    }

    /**
     * Callback to sync similarity with APAC
     *
     * @param submissionId
     * @param similarities
     * @throws URISyntaxException
     * @throws ConnectException
     */
    private void postSimilarity(List syncValues) throws URISyntaxException, ConnectException {
        String response = restTemplate.postForObject(baseUrl, syncValues, String.class);
        logger.debug(response);
    }

    public void setDebugBaseUrl(String url) {
        this.baseUrl = url;
    }

    /**
     * Entity to wrap one similarity info for APAC
     */
    public class RequestEntity {

        String baseUuid;
        Double similarity = Double.MIN_VALUE;
        public List similarSubmissions;

        public RequestEntity(String id) {
            this.baseUuid = id;
        }

        public RequestEntity(String id, List data) {
            this.baseUuid = id;
            this.similarSubmissions = data;
        }

        public String getBaseUuid() {
            return baseUuid;
        }

        public void setBaseUuid(String baseUuid) {
            this.baseUuid = baseUuid;
        }

        public Double getSimilarity() {
            return similarity;
        }

        public void setSimilarity(Double similarity) {
            this.similarity = similarity;
        }

        public List getSimilarSubmissions() {
            return similarSubmissions;
        }

        public void setSimilarSubmissions(List similarSubmissions) {
            this.similarSubmissions = similarSubmissions;
        }

        @Override
        public String toString() {
            return "RequestEntity{" + "baseUuid=" + baseUuid + ", similarity=" + similarity + "}";
        }

    }

    public class SimilarityEntity {

        String uuid;
        Double similarity;

        public SimilarityEntity(String uuid, Double similarity) {
            this.uuid = uuid;
            this.similarity = similarity;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public Double getSimilarity() {
            return similarity;
        }

        public void setSimilarity(Double similarity) {
            this.similarity = similarity;
        }

    }

}
