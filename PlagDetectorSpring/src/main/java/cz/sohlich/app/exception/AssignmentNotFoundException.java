/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author radek
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Assignment not found")
public class AssignmentNotFoundException extends Exception {

    /**
     * Creates a new instance of <code>NoItemException</code> without detail
     * message.
     */
    public AssignmentNotFoundException() {
    }

    /**
     * Constructs an instance of <code>NoItemException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public AssignmentNotFoundException(String msg) {
        super(msg);
    }
}
