/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service;

import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.util.FileHandler;
import java.io.IOException;

/**
 *
 * @author radek
 */
public interface SubmissionHelper {

    void process(FileHandler fHandler, Assignment assignment, String owner, String submission) throws IOException;
    
}
