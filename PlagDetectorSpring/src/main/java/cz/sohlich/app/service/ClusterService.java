/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service;

import cz.fai.utb.data.dto.SimplifiedFileDTO;
import java.util.List;

/**
 *
 * @author radek
 */
public interface ClusterService {

    List<List<SimplifiedFileDTO>> clusterize(List<SimplifiedFileDTO> files);

    Double getEpsilon();

    Integer getMinPoints();

    void setEpsilon(Double epsilon);

    void setMinPoints(Integer minPoints);
    
}
