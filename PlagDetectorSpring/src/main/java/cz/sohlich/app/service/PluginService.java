/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service;

import cz.fai.utb.lang.api.LanguageProcessor;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author radek
 */
public interface PluginService {

    String[] extensionByLanguage(String lang);

    LanguageProcessor getProcessorByLanguage(String lang);

    Set getSupportedLangs();

    boolean isSupportedLang(String lang);

    @PostConstruct
    void load();

    void onApplicationEvent(ApplicationEvent e);
    
}
