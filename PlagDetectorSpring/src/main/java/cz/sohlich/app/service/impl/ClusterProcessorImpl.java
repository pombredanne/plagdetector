/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.service.impl;

import cz.fai.utb.cluster.SimpleDBSCANClusterer;
import cz.fai.utb.data.dto.SimplifiedFileDTO;
import cz.sohlich.app.service.ClusterService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Radek
 */
@Component
public class ClusterProcessorImpl implements ClusterService {

    @Value("${pg.cluster.epsilon}") Double epsilon;
    @Value("${pg.cluster.minpoints}") Integer minPoints;

    @Override
    public List<List<SimplifiedFileDTO>> clusterize(List<SimplifiedFileDTO> files) {

        List<List<SimplifiedFileDTO>> returnList = new ArrayList<>();
        
        SimpleDBSCANClusterer clusterer = new SimpleDBSCANClusterer(epsilon, minPoints);
        List<Cluster<SimplifiedFileDTO>> clusters = clusterer.clusterize(files);
        
        clusters.stream().forEach((cluster) -> {
            returnList.add(cluster.getPoints());
        });

        return returnList;
    }

    @Override
    public Double getEpsilon() {
        return epsilon;
    }

    @Override
    public void setEpsilon(Double epsilon) {
        this.epsilon = epsilon;
    }

    @Override
    public Integer getMinPoints() {
        return minPoints;
    }

    @Override
    public void setMinPoints(Integer minPoints) {
        this.minPoints = minPoints;
    }

}
