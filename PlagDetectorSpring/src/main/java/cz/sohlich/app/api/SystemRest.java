/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.api;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.wordnik.swagger.annotations.ApiOperation;
import cz.sohlich.app.service.PluginService;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author radek
 */
@RestController
@RequestMapping("/system")
public class SystemRest {

    @Autowired
    Environment env;
    
    @Autowired
    PluginService pluginHolder;
    
    @Autowired @Qualifier(value = "metric-registry") MetricRegistry metrics;
    @Autowired @Qualifier(value = "health-check-registry") HealthCheckRegistry health;

    @ApiOperation(value = "List all suported languages")
    @RequestMapping(value = "/langs",method = RequestMethod.GET)
    public Set supportedLangs() {
        return pluginHolder.getSupportedLangs();
    }
    
    @ApiOperation(value = "External system properties")
    @RequestMapping(value = "/properties",method = RequestMethod.GET)
    public Map properties() {
        Map porpertiesMap = new HashMap();
        porpertiesMap.put("pg.mongo.host", env.getProperty("pg.mongo.host"));
        porpertiesMap.put("apac.sync.url", env.getProperty("apac.sync.url"));
        return porpertiesMap;
    }
    
    @ApiOperation("Metrics for system")
    @RequestMapping(value = "/metrics",method = RequestMethod.GET)
    public Map metricsCheck() {        
        return metrics.getMetrics();
    }
    
    @ApiOperation("Helth checks of system")
    @RequestMapping(value = "/health",method = RequestMethod.GET)
    public SortedMap healthCheck() {
        return health.runHealthChecks();
    }
}
