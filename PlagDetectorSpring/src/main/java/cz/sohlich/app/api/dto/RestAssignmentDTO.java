/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.api.dto;

/**
 *
 * @author radek
 */
public class RestAssignmentDTO {
    String owner;
    String name;
    String assignmentId;
    String lang;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public String toString() {
        return "RestAssignmentDTO{" + "owner=" + owner + ", name=" + name + ", assignmentId=" + assignmentId + ", lang=" + lang + '}';
    }
    
    
    
}
