/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.api;

import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.entity.dto.FileDTO;
import cz.sohlich.app.entity.dto.SubmissionDTO;
import cz.sohlich.app.repository.AssignmentRepository;
import cz.sohlich.app.repository.FileRepository;
import cz.sohlich.app.exception.AssignmentNotFoundException;
import cz.sohlich.app.service.SubmissionHelper;
import cz.sohlich.util.FileHandler;
import cz.sohlich.util.constant.AssignmentStatus;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Radek
 */
@RestController
@RequestMapping("/rest/submission")
public class SubmissionRest {

    Logger log = LoggerFactory.getLogger(SubmissionRest.class);

    @Autowired FileRepository fileRepo;
//    @Autowired PluginHolder pluginHolder;
    @Autowired AssignmentRepository assignmentRepo;
    @Autowired SubmissionHelper submissionHelper;

    //Values
    @Value("${pg.filesystem.temp}") private String tempFolder;
    @Value("${pg.filesystem.assignment.path}") private String assignmentFolder;

    /**
     * Creates submission and upload file. Needs: Content-Type:
     * multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
     *
     * ----WebKitFormBoundary7MA4YWxkTrZu0gW Content-Disposition: form-data;
     * name="submission-meta"
     *
     * { "owner": "rada", "assignmentId": "54f205ff3ef60a124d826699",
     * "identificator": "62" } ----WebKitFormBoundary7MA4YWxkTrZu0gW
     * Content-Disposition: form-data; name="submission-data"; filename="62.zip"
     * Content-Type: application/zip ----WebKitFormBoundary7MA4YWxkTrZu0gW
     *
     * @param submissionMetaString json data { "owner": "rada", "assignmentId":
     * "54f205ff3ef60a124d826699", "identificator": "62" }
     * @param submission
     * @return
     * @throws IOException
     * @throws Exception
     */
    @ApiOperation("Create submission with file upload")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "No assignment for the assignmentId"),
        @ApiResponse(code = 422, message = "IOException occurs while saving file"),})
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE) //, consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE
    public List<FileDTO> createSubmission(@RequestParam("submission-meta") String submissionMetaString,
            @RequestPart("submission-data") MultipartFile submission) throws AssignmentNotFoundException, IOException { //

        //Helps to bypass json in multipart
        ObjectMapper mapper = new ObjectMapper();
        SubmissionDTO submissionMeta = mapper.readValue(submissionMetaString, SubmissionDTO.class);

        log.info("Accepting request {}", submissionMetaString);

//        Select assignment from repo
//        If not assignment throws exception
        Assignment assignment = assignmentRepo.findOneByAssignmentId(submissionMeta.getAssignmentId())
                .orElseThrow(AssignmentNotFoundException::new);

        byte[] file = IOUtils.toByteArray(submission.getInputStream());

        log.info("Saving file with length {}", file.length);

        //Save zip file
        FileHandler fHandler = new FileHandler(assignmentFolder, tempFolder);
        fHandler.saveFile(file);

        submissionHelper.process(fHandler, assignment, submissionMeta.getOwner(), submissionMeta.getIdentificator());

        List<FileDTO> assignmentList = StreamSupport.stream(fileRepo.findByOwner(submissionMeta.getOwner()).spliterator(), false)
                .map(FileDTO::new)
                .collect(Collectors.toList());

        //Mark assignment to be checked again
        assignment.setStatus(AssignmentStatus.NEW);
        assignmentRepo.save(assignment);

        return assignmentList;

    }

    /**
     * Creates mongo db submission record without uploading file. The file is
     * present on FS in #tempFolder path.
     *
     * @param submission
     * @return
     * @throws IOException
     * @throws cz.sohlich.app.exception.AssignmentNotFoundException
     * @throws Exception
     */
    @ApiOperation("Create submission from file already located on server")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "No assignment for the assignmentId"),
        @ApiResponse(code = 422, message = "IOException occurs while saving file"),})
    
    @RequestMapping(value = "/create", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE) //, consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE
    public List<FileDTO> createSubmissionFilePresent(@RequestBody SubmissionDTO submission) throws IOException, AssignmentNotFoundException {

        //If not assignment throws exception
        Assignment assignment = assignmentRepo.findOneByAssignmentId(submission.getAssignmentId())
                .orElseThrow(AssignmentNotFoundException::new);

        log.info("Submission {} is being processed.", submission.toString());

        //Save zip file
        FileHandler fHandler = new FileHandler(assignmentFolder, tempFolder, submission.getIdentificator());

        submissionHelper.process(fHandler, assignment, submission.getOwner(), submission.getIdentificator());

        List<FileDTO> assignmentList = StreamSupport.stream(fileRepo.findByOwner(submission.getOwner()).spliterator(), false)
                .map(FileDTO::new)
                .collect(Collectors.toList());

        //Mark assignment to be checked again
        assignment.setStatus(AssignmentStatus.NEW);
        assignmentRepo.save(assignment);

        return assignmentList;
    }
    
    
    
    //Testable design

    public void setTempFolder(String tempFolder) {
        this.tempFolder = tempFolder;
    }

    public void setAssignmentFolder(String assignmentFolder) {
        this.assignmentFolder = assignmentFolder;
    }
    

}
