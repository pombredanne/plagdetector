/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.api;

import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import cz.sohlich.app.service.SimilarityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author Radek
 */
@RestController
@RequestMapping("/rest/similarity")
public class SimilarityRest {

    @Autowired SimilarityService submissionHelper;

    @ApiOperation("Similarity for assignment")
    @RequestMapping(value = "/assignment/{assignmentId}", method = RequestMethod.GET)
    public List getAssignmentInfo(@PathVariable("assignmentId") String assignmentId) {
        return submissionHelper.getAssignmentSimilaritiesInfo(assignmentId);
    }

    @ApiOperation("Similarity for submission")
    @RequestMapping(value = "/submission/{submissionId}", method = RequestMethod.GET)
    public Double getInfo(@PathVariable("submissionId") String submissionId) {
        return submissionHelper.getSubmissionInfo(submissionId);
    }

}
