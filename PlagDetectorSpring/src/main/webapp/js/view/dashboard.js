'use strict';

angular.module('myApp.dashboard', ['ngRoute', 'chart.js', 'ngResource'])
        .controller('DashboardController', ['$scope', 'AssignmentRest', 'DatetimeFactory', function ($scope, AssignmentRest, DatetimeFactory) {


                $scope.stats = {};
                $scope.tableData = {};
                $scope.labels = [];
                $scope.data = [];
                $scope.series = ['Assignment count'];

                $scope.stats = AssignmentRest.dashboard({}, {'Owner': 'rsohlich'}, function () {

                    $scope.stats.assignments.forEach(function (item) {
                        if (typeof $scope.tableData[item.assignmentId] === 'undefined') {
                            $scope.tableData[item.assignmentId] = {
                                assignment: item,
                                count: 0,
                                suspected: 0
                            };
                        }
                    });

                    $scope.stats.submissions.forEach(function (item) {
                        $scope.tableData[item.assignmentId].count += item.count;
                    });
                    
                    $scope.stats.suspecteds.forEach(function (item) {
                        $scope.tableData[item.assignmentId].suspected += item.count;
                    });
                });

                $scope.onClick = function (points, evt) {
                    console.log(points, evt);
                };
            }]);