'use strict';

var baseUrl = 'http://localhost:8080/PlagDetector/rest';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.dashboard',
    'myApp.view2',
    'myApp.version',
    'myApp.services',
    'ngResource',
    'chart.js'
]).
        config(['$routeProvider', function ($routeProvider) {

                $routeProvider.when('/dashboard', {
                    templateUrl: 'partials/dashboard.html',
                    controller: 'DashboardController'
                });
                $routeProvider.otherwise({redirectTo: '/dashboard'});
            }]);
