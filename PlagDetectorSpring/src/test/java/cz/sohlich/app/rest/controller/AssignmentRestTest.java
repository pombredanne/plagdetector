/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.sohlich.app.rest.controller;

import cz.sohlich.app.api.AssignmentRest;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import cz.sohlich.app.GenericTest;
import cz.sohlich.app.entity.dao.Assignment;
import cz.sohlich.app.repository.AssignmentRepository;
import cz.sohlich.app.repository.FileRepository;
import cz.sohlich.app.service.PluginService;
import cz.sohlich.app.service.SubmissionHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author radek
 */
public class AssignmentRestTest extends GenericTest {

    @Mock AssignmentRepository assignmentRepo;
    @Mock FileRepository fileRepo;
    @Mock SubmissionHelper submissionHelper;
    @Mock PluginService pluginHolder;

    @InjectMocks AssignmentRest assignRest;

    public AssignmentRestTest() throws IOException {
        super();
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(assignRest).build();
        RestAssuredMockMvc.mockMvc = mockMvc;
        presetBeans();
    }

    public void presetBeans() {
        //Preset the return values
        Mockito.when(pluginHolder.isSupportedLang(anyString())).thenReturn(Boolean.TRUE);
        Mockito.when(assignmentRepo.save(any(Assignment.class))).thenReturn(new Assignment("1234", "rada", "C", "Test", "NEW", new Date()));
    }

    @Test
    public void testCreateAssignment() {
        given().body("{\"owner\":\"rada\",\"name\": \"CHistory\",\"lang\": \"C\",\"assignmentId\": \"0\"}")
                .contentType("application/json")
                .when().
                post("/rest/assignment/create").
                then().
                statusCode(HttpServletResponse.SC_OK);
    }
    
    
    @Test
    public void testGetAll(){
    given().
                when().
                get("/rest/assignment/all/rada").
                then().
                statusCode(HttpServletResponse.SC_OK);
    }

}
