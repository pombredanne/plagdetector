/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fai.utb.py.api;


import cz.fai.utb.lang.api.LanguageProcessor;
import cz.fai.utb.lang.api.ParseResultWrapper;
import cz.fai.utb.lang.api.Preprocessor;
import cz.fai.utb.py.lexer.Python3LexerImpl;
import cz.fai.utb.py.preprocesor.RegexPreprocesor;


/**
 *
 * @author Radek
 */
public class PythonProcessor implements LanguageProcessor {

    @Override
    public ParseResultWrapper parseSource(String file) {
        Preprocessor processor = new RegexPreprocesor(file);
        file = processor
                .removeComments()
                .process();

        Python3LexerImpl lexer = new Python3LexerImpl();
        lexer.nGramize(file, 4);
        
        ParseResultWrapper result = new ParseResultWrapper(lexer.getnGrams(),new double[]{lexer.getLoopCount(),lexer.getIdentifierCount(),lexer.getIfCount()});
       return result;
    }

    @Override
    public String getLang() {
        return "PYTHON";
    }

    @Override
    public String[] getExtensions() {
       return new String[]{"py"};
    }
}
