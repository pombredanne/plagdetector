/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fai.utb.py.api;

import cz.fai.utb.lang.api.ParseResultWrapper;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author radek
 */
public class PythonProcessorTest {

    public PythonProcessorTest() {
    }

    /**
     * Test of parseSource method, of class PythonProcessor.
     *
     * @throws java.io.IOException
     */
    @Test
    public void trivialTest() throws IOException {
        InputStream is = ClassLoader.getSystemResourceAsStream("test/test_file.py");
        String content = IOUtils.toString(is);
        PythonProcessor instance = new PythonProcessor();
        ParseResultWrapper result = instance.parseSource(content);
        assertTrue("No tokens parsed", result.getnGrams().size() > 0);
        System.out.println("Parsed result:" + result.toString());
    }

    /**
     * Test of getLang method, of class PythonProcessor.
     */
    @Test
    public void testGetLang() {
        System.out.println("getLang");
        PythonProcessor instance = new PythonProcessor();
        String expResult = "PYTHON";
        String result = instance.getLang();
        assertEquals(expResult, result);
    }

    /**
     * Test of getExtensions method, of class PythonProcessor.
     */
    @Test
    public void testGetExtensions() {
        System.out.println("getExtensions");
        PythonProcessor instance = new PythonProcessor();
        String[] expResult = {"py"};
        String[] result = instance.getExtensions();
        assertArrayEquals(expResult, result);
    }

}
