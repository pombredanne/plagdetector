/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.fai.utb.algorithms.winnowing;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Radek
 */
public class WinnowFingerPrint {

    List<Hash> fingerprint = new  LinkedList<>();
    int[] fullDoc;
    
    
    
    public void putHash(int index,int hashVal){
        fingerprint.add(new Hash(index, hashVal));
    }
    
    public class Hash {
        int index;
        int hashVal;

        public Hash(int index, int hashVal) {
            this.index = index;
            this.hashVal = hashVal;
        }

        @Override
        public String toString() {
            return "Hash{" + "index=" + index + ", hashVal=" + hashVal + '}';
        }
        
        
    }

    public void debugPrint(PrintStream ps){ 
        for (Hash p : fingerprint) {
            ps.println(p.toString());
        }
    }
    
    
    
    
}
